// git包
const downLoad = require('download-git-repo');
// 动画
const ora = require('ora');

const symbol =require('log-symbols');

const cp = require('child_process');

const  path = require("path");

const  process = require("process");


let clone = true;

let downGit = (name,type) => {
    // console.log("name:"+name);
    let url = null;
    if(type ==='vue2.0'){
        url='direct:https://gitee.com/zhoalei3/vue2-template.git'
    }else if(type ==='vue3.0jsx'){
        url='direct:https://gitee.com/zhoalei3/vue3-template-jsx.git'
    }else if(type ==='vue3.0'){
        url='direct:https://gitee.com/zhoalei3/vue3-template.git'
    }
    else if(type==='react'){
        url='direct:https://gitee.com/zhoalei3/react-template.git'
    }else if(type==='nuxt'){
        url='direct:https://gitee.com/zhoalei3/nuxt-template.git'
    }
    else {
        url='direct:https://gitee.com/zhoalei3/koa-template.git'
    }
    // let url= (type ==="vue")?"nykee/vue-template":"nykee/antd-template";
    let spinner = ora('正在拉取模板.../downloading template...');
    spinner.start();
    downLoad(url, name, {
        clone
    }, err => {

        if(err){
            console.log(symbol.error,err);
            spinner.stop();
            process.exit(1);
        }
        else {
            spinner.stop();
            // console.log(symbol.success,"开始安装依赖...");
            // let spinner2 = ora('开始安装依赖...');
            // let  url = path.resolve('./')+"/"+name;
            // cp.exec('yarn',{cwd:url}, (error, stdout, stderr) => {
            //     if (error) {
            //         console.log(symbol.success,"依赖安装失败");
            //     } else {
            //         console.log(symbol.success,"依赖安装成功");
            //
            //     }
            //     spinner2.stop();
            //     console.log(stdout);
            //     console.log(symbol.success,"项目创建成功");
            //     process.exit(1);


            // });
            console.log(symbol.success,"项目创建成功/project create success");
            process.exit(1);

        }

    })

};
module.exports = downGit;