const inquirer = require('inquirer');
let optionsArr = [{
    type: 'list',
    name: "type",
    message: "请选择你想创建的模板类型!/choose the template u want!",
    choices: [
        "vue2.0",
        "vue3.0jsx",
        "vue3.0",
        "react",
        "nuxt-template",
        'koa-template'
    ],
    default: 'vue3.0jsx'
}];

module.exports = () => {
    return new Promise((resolve)=>{
        inquirer
            .prompt(optionsArr).then(answers=>{
            resolve(answers);
        })
    });
};